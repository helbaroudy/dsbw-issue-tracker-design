import java.util.Date

/**
 * Created by hussein on 26/05/14.
 */

class User(val username: String, val name: String) {
  var myIssues: Array[Issue] = ()
  //Mine
  var assigned_issues: Array[Issue] = ()
}

class Issue(val id: Int, var title: String, val owner: User, val priority: Priority, val category: Category,
            val author: User, var assignee: User, val createAction: CreateAction) {
  var description: String = ""
  var history: List[Action] = List(createAction)

  def filterComments(actions: List[Action]) = ??? //Para calcular la relacion "comments"

  var comments: List[Action] = {
    val filteredActions = filterComments(history)
  }


}

case class Priority(name: String)

case class Category(name: String)

class Comment(val user: User, val date: Date, var text: String = "") {
}

trait Action {
  val date: Date
}

class CreateAction(val d: Date) extends Action {
  override val date: Date = d
}

class UpdateAction(val d: Date) extends Action {
  override val date: Date = d
}

class CommentAction(val d: Date) extends Action {
  override val date: Date = d

}

